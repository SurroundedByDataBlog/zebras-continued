# Zebras (continued) - my first data collection project

## Library imports
library(tidyverse)
library(patternplot)
library(jpeg)

# Load fonts
library(extrafont)
# Execute next line once to import system fonts
font_import(paths = c("."), recursive = F, prompt = F, pattern = "*.ttf")
loadfonts()


## Attribution
# The Elizajane font was found here: https://www.1001fonts.com/children+handwriting-fonts.html


## Load data
data_raw = read.csv("Dieren.csv")
data_totaal = aggregate(n ~ dier + animal, sum, data = data_raw)
data_top5 = data_totaal[order(data_totaal$n, decreasing = T), ][1:5, ]
data_top5$animal = factor(data_top5$animal, levels = data_top5$animal)
data_totaal = aggregate(n ~ dier + animal, sum, data = read.csv("Dieren.csv"))


# Load images
img_zebra = readJPEG("patterns/zebra_pattern.jpg")
img_wildebeest = readJPEG("patterns/wildebeest_pattern.jpg")
img_olifant = readJPEG("patterns/elephant_pattern.jpg")
img_dikdik = readJPEG("patterns/dikdik_pattern.jpg")
img_wildzwijn = readJPEG("patterns/warthog_pattern.jpg")
img_list = list(img_zebra, img_wildebeest, img_olifant, img_dikdik, img_wildzwijn)


## Plotting
animals_plot = imagebar(data = NULL, x=data_top5$animal, y=data_top5$n, pattern.type=img_list, frame.size = 0, label.size = 4) +
  ylim(c(0,450)) +
  labs(title = "Top 5 animals seen on safari", subtitle = "January 1996, Amboseli National Park, Kenya\n") +

  theme_minimal(20) +
  theme(text = element_text(family="elizajane"),
        plot.title = element_text(hjust = 0.5),
        plot.subtitle = element_text(hjust = 0.5),
        axis.text.y=element_blank(),
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        plot.margin = unit(c(1,1,0.5,0.5), "cm"))

print(animals_plot)
ggsave(animals_plot, file="animals_plot.jpg", width=13, height=10, units="cm", scale=1.75)
