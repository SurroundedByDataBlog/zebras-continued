# Zebras Continued

My very first data collection project was a tally of the animals we saw on a safari trip in 1996. <br/>
Read the full story in [my blog](https://surroundedbydata.netlify.com/post/zebras-continued/).
The code in this project produces the following graph:

<img src=animals_plot.jpg width="500" height="400" />
